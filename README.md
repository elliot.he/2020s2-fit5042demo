# 2020S2-FIT5042Demo

This repository is just for FIT5042 2020 S2 demo purpose.

## Environment Setup

1. Download and install Eclipse 2019-12 for Enterprise Java Developers: https://www.eclipse.org/downloads/packages/release/2019-12/r
2. Download and unzip Glassfish 5: http://download.oracle.com/glassfish/5.0/release/glassfish-5.0.zip
3. Install JBoss Tools from Eclipse Marketplace
4. Install Glassfish Tools: http://download.eclipse.org/glassfish-tools/1.0.0/repository
5. Install Sapphire: http://download.eclipse.org/sapphire/9.1.1/repository

## Set environment variables

You need to make sure the Derby_Home and Java_Home environment variables are correctly setup in your system. Please be adviced that the JAVA_HOME location on your machine might be different, please check it individually.

### Mac
```bash
vi ~/.bash_profile
```
Put the content below to the bash file
```
export DERBY_HOME=[Your workspace path]/JEE_Library/db-derby-10.2.2.0-bin
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_221.jdk/Contents/Home
```
Then press ESC button on your keyboard, and run this command.
```bash
:wq
```
Alternatively, if somehow your environment variables are not able to be correctly set, you can simply add these two lines at the top of your startNetworkServer script file.

### Windows

Just go to environment variables settings from Windows menu.

Create two new variables:

```
DERBY_HOME
[Your workspace path]/JEE_Library/db-derby-10.2.2.0-bin

JAVA_HOME
/Library/Java/JavaVirtualMachines/jdk1.8.0_221.jdk/Contents/Home
```

## Spring MVC prerequesite setup - MacOS
### Install Homebrew

1. Run the command `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
2. Update the brew by calling `brew update`

### Install Maven using Homebrew

1. Run the command `brew install maven`

### Update Maven using Homebrew

1. Unlink the old version of the Maven using `brew unlink maven`
2. Then run the command to install the latest Maven by `brew install maven`
3. Check the Maven version by typing `mvn -v` or `mvn -version`

### Install Tomcat 9 using Homebrew

1. Run the command `brew install tomcat@9`
2. Use `brew ls tomcat@9` to check all the related directory locations
3. Set the Tomcat home location to follow with the pattern of `/usr/local/Cellar/tomcat/<Tomcat Version>/libexec/`, in this case, we will simply map it to `/usr/local/Cellar/tomcat/9.0.38/libexec/`

### Change the port number for Tomcat server

1. In the terminal, type `nano /usr/local/Cellar/tomcat/9.0.38/libexec/conf/server.xml`
2. Then, find the `Connector` tag
```bash
<Connector port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />
```
3. Change it to
```bash
<Connector port="8089" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />
```

### Uninstall Homebrew

1. Run the command `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall.sh)"`


## Spring MVC prerequesite setup - Windows

### Set Maven Home

1. Opening up the system properties. 
2. Selecting the “Advanced” tab, and the “Environment Variables” button.
3. Adding or selecting the PATH variable in the user variables with the value C:\Program Files\apache-maven-3.6.3\bin (please change the path to where you installed your Maven)
### Download links for Windows

1. Maven: https://maven.apache.org/install.html
2. Tomcat: https://www.liquidweb.com/kb/installing-tomcat-9-on-windows/
3. MySQL: https://dev.mysql.com/downloads/mysql/
Workbench: https://dev.mysql.com/downloads/workbench

## Contributing
Pull requests are welcomed. Please do not make any push to this repository, any changes in this repository won't be marked for this unit.
